package com.example.a20230120_andrewbosco_nycschools.di

import com.example.a20230120_andrewbosco_nycschools.model.Network
import com.example.a20230120_andrewbosco_nycschools.model.ReposImpl
import com.example.a20230120_andrewbosco_nycschools.model.Repository
import com.example.a20230120_andrewbosco_nycschools.viewmodel.SchoolViewModelProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepoModule {
    @Singleton
    @Provides
    fun providesRepository(): Repository {
        return ReposImpl(Network.getInstance())
    }
}

@InstallIn(ActivityComponent::class)
@Module
object ViewModelModule{
    @Provides
    fun providesVM(repository: Repository): SchoolViewModelProvider {
        return SchoolViewModelProvider(repository)
    }
}