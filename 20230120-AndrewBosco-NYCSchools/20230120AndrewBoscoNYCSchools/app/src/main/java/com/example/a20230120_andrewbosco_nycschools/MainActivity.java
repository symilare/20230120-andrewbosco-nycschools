package com.example.a20230120_andrewbosco_nycschools;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.example.a20230120_andrewbosco_nycschools.view.Listener;
import com.example.a20230120_andrewbosco_nycschools.view.SchoolDetails;
import com.example.a20230120_andrewbosco_nycschools.view.SchoolDisplay;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity implements Listener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState==null)
            showDisplayFragment();
    }

    private void showDisplayFragment() {
        SchoolDisplay schoolDisplayFragment = new SchoolDisplay();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, schoolDisplayFragment)
                .commit();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("Initialize", false);
    }

    @Override
    public void openDetails(String dbn, String name, String loc, String email, String phone) {

        SchoolDetails schoolDetailsFragment = new SchoolDetails().getInstance(dbn, name, loc, email, phone);
        //schoolDetailsFragment.setRetainInstance(true);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, schoolDetailsFragment)
                .addToBackStack(null)

                .commit();
    }

}