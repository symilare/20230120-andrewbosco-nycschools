package com.example.a20230120_andrewbosco_nycschools.network;

import com.example.a20230120_andrewbosco_nycschools.model.SchoolItem;
import com.example.a20230120_andrewbosco_nycschools.model.ScoreItem;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

public class SchoolRepositoryImpl implements SchoolRepository {

    private SchoolService service;

    public SchoolRepositoryImpl(SchoolService service) {
        this.service = service;
    }

    @Override
    public Single<List<SchoolItem>> getAllSchools() {
        return service.getSchools();
    }

    @Override
    public Single<List<ScoreItem>> getAllScores() {
        return service.getSatScores();
    }
}
