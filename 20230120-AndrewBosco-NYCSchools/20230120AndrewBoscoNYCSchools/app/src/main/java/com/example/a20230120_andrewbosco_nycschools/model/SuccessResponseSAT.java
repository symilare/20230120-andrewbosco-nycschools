package com.example.a20230120_andrewbosco_nycschools.model;

public class SuccessResponseSAT extends UIState{
    private NYCSATResponse data;

    public NYCSATResponse getData() {
        return data;
    }

    public void setData(NYCSATResponse data) {
        this.data = data;
    }
}