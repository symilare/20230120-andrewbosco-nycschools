package com.example.a20230120_andrewbosco_nycschools.model;

import io.reactivex.rxjava3.core.Single;


public class ReposImpl implements Repository {

    private Network network;

    public ReposImpl(Network network){
        this.network = network;
    }

    @Override
    public Single<UIState> getSchoolList() {
        return network.getSERVICE().getSchoolList()
                .map(nycSchoolResponses -> {
                    SuccessResponseSchool result = new SuccessResponseSchool();
                    result.setData(nycSchoolResponses);
                    return result;
                });
    }

    @Override
    public Single<UIState> getSchoolDetails(String dbn) {
        return network.getSERVICE().getSatList()
                .map(nycSchoolSats -> {
                    NYCSATResponse satSchool = new NYCSATResponse();
                    for (NYCSATResponse sat :
                            nycSchoolSats) {
                        if (sat.getDbn().equals(dbn))
                            satSchool = sat;
                    }
                    SuccessResponseSAT result = new SuccessResponseSAT();
                    result.setData(satSchool);
                    return result;
                });
    }
}