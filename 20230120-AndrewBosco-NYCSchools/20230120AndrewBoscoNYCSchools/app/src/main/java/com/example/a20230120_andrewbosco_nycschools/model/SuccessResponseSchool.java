package com.example.a20230120_andrewbosco_nycschools.model;


import java.util.List;

public class SuccessResponseSchool extends UIState{
    private List<NYCSchoolResponse> data;

    public List<NYCSchoolResponse> getData() {
        return data;
    }

    public void setData(List<NYCSchoolResponse> data) {
        this.data = data;
    }
}