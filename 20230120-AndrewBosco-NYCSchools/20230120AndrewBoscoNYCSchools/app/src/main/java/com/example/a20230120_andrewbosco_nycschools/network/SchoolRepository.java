package com.example.a20230120_andrewbosco_nycschools.network;

import com.example.a20230120_andrewbosco_nycschools.model.SchoolItem;
import com.example.a20230120_andrewbosco_nycschools.model.ScoreItem;

import java.util.List;

import io.reactivex.rxjava3.core.Single;

public interface SchoolRepository {
    Single<List<SchoolItem>> getAllSchools();
    Single<List<ScoreItem>> getAllScores();
}
