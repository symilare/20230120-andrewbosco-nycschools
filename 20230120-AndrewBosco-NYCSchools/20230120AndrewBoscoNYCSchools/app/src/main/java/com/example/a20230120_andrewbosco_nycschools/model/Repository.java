package com.example.a20230120_andrewbosco_nycschools.model;

import io.reactivex.rxjava3.core.Single;
public interface Repository {
    Single<UIState> getSchoolList();
    Single<UIState> getSchoolDetails(String input);
}
