package com.example.a20230120_andrewbosco_nycschools.network;

import com.example.a20230120_andrewbosco_nycschools.model.SchoolItem;
import com.example.a20230120_andrewbosco_nycschools.model.ScoreItem;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;

public interface SchoolService {

    // base URL = https://data.cityofnewyork.us/resource/
    public final static String BASE_URL = "https://data.cityofnewyork.us/resource/";
    static String SCHOOLS = "s3k6-pzi2.json";
    static String SCORES = "f9bf-2cp4.json";



    @GET(SCHOOLS)
    public Single<List<SchoolItem>> getSchools();

    @GET(SCORES)
    public Single<List<ScoreItem>> getSatScores();
}
