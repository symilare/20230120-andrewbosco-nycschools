package com.example.a20230120_andrewbosco_nycschools.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.a20230120_andrewbosco_nycschools.model.Repository;

import javax.inject.Inject;

import io.reactivex.rxjava3.annotations.NonNull;

public class SchoolViewModelProvider implements ViewModelProvider.Factory {

    private Repository repository;

    @Inject
    public SchoolViewModelProvider(Repository repository){
        this.repository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new SchoolViewModel(repository);
    }
}