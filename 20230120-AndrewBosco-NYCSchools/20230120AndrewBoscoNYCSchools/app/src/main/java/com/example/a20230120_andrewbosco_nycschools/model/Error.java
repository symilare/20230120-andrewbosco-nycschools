package com.example.a20230120_andrewbosco_nycschools.model;

public class Error extends UIState {
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}